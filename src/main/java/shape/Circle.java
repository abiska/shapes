package shape;


public class Circle extends Shape{
    
    //Method(s)
    @Override
    public double getArea(double d){
        return Math.PI*Math.pow(d, 2);
    }
    
    @Override
    public double getPerimeter(double d){
        return 2*Math.PI*d;
    }    
}
