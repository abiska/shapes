package shape;

import java.text.DecimalFormat;

public class ShapesSimulation {

    public static void main(String[] args) {
        
        DecimalFormat df = new DecimalFormat("#.#");
        
        Shape circle = new Circle();
        Shape square = new Square();
        
        System.out.println("Area of Circle with a radius of 8cm is " + df.format(circle.getArea(8.0)) + " cm2" + 
                            "\nPerimeter of Circle with a radius of 8cm is " + df.format(circle.getPerimeter(8.0)) + " cm");
        
        System.out.println("\nArea of Square with a side of 6cm is " + df.format(square.getArea(6.0)) + " cm2" + 
                            "\nPerimeter of Square with a side of 6cm is " + df.format(square.getPerimeter(6.0)) + " cm");
        
    }
    
}
